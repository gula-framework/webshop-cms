<?php

namespace Gula\WebsiteCms\Controllers;


use App\Http\Controllers\Controller;

class ShopPaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ROLE_ADMIN');
    }

}
