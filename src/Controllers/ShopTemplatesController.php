<?php


namespace Gula\WebsiteCms\Controllers;

use App\Http\Controllers\Controller;
use Gula\WebsiteCms\Models\ShopTemplates;

class ShopTemplatesController extends Controller
{
    protected $table = 'shop_templates';

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:ROLE_ADMIN');
    }

    public function edit(string $name_constant)
    {
        $mdl = new ShopTemplates();
        $record = $mdl->getOne($name_constant);

        $scope = [
            'table' => $mdl->getTableName(),
            'title' => 'Sjabloon: ' . $name_constant,
            'icon' => 'https://cms.gula.nl/resizer/36x36/cms/icons/edit.png',
        ];

        $viewFile = 'website-cms::edit_' . $mdl->getTableName();

        return view($viewFile, compact('record', 'scope'));
    }



}
