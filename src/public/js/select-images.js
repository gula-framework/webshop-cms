function selectImage(max, imageType, imageId) {
    $('#selectImages').modal();
}

function setImage(img) {
    $('#selectImages').modal('hide');
    $('#image').val(img);
    $('#category_image').attr('src', "{{env('PATH_CDN')}}300x300/{{env('APP_CDN')}}/categorie/" + img);
}

function setMultipleImage(img) {
    debugger;
    var idImage = parseInt($('#imageId').val()) + 1;
    $('#imageId').val(idImage);
    $('#productImages').append('<input type=hidden name="image_' + idImage + '" value="' + img + '" data-tableid=""  />');
    $('#productImages').append('<div class="col-2"><img src="' + path_cdn + '300x300/' + app_cdn + '/product/' + img + '" class=""></div>');
    $('#selectImages').modal('hide');
}
