<?php

namespace Gula\WebsiteCms;

use Illuminate\Support\ServiceProvider;

class CmsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadViewsFrom(__DIR__ . '/views', 'website-cms');
        $this->publishes([__DIR__ . '/public/less' => public_path('less')], 'public');
        $this->publishes([__DIR__ . '/public/js' => public_path('js')], 'public');

    }
}
