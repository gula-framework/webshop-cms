<?php


namespace Gula\WebsiteCms\Controllers;


use Gula\ListFilter\ListFilterController;
use Illuminate\Support\Facades\DB;

class ShopOrdersController
{
    protected $table = 'shop_orders';

    public function __construct()
    {
        //        $this->middleware('auth');
    }

    public function list()
    {
        $orders = DB::table($this->table)
            ->select([$this->table.'.*', $this->table.'.id AS id_order', 'users.*'])
            ->leftJoin('users', 'users.id', '=', $this->table . '.id_user')
            ->where(['deleted' => false])
            ->orderBy($this->table . '.id', 'DESC')
            ->get();

        $viewFile = 'website-cms::list_' . $this->table;

        return view($viewFile, compact('orders'));
    }

}
