<?php
Route::group(['middleware' => 'web', 'namespace' => 'Gula\WebsiteCms\Controllers'], function()
{
    Route::get('/cms/image_manager/{folder}/{subfolder}', ['uses' => 'WebsiteCmsController@imageManager']);
    Route::get('/cms/image_manager/{folder}', ['uses' => 'WebsiteCmsController@imageManager']);
    Route::get('/cms/image_manager', ['uses' => 'WebsiteCmsController@imageManager']);

    Route::get('cms/shop_categories/edit/{id}', ['uses' => 'ShopCategoriesController@edit']);
    Route::get('cms/shop_categories/list', ['uses' => 'ShopCategoriesController@list']);

    Route::get('cms/shop_templates/edit/{name_constant}', ['uses' => 'ShopTemplatesController@edit']);

    Route::get('cms/pages/edit/{id}', ['uses' => 'PagesController@edit']);
    Route::post('cms/pages/store', ['uses' => 'PagesController@store']);

    Route::post('cms/shop_products/store', ['uses' => 'ShopProductsController@store']);
    Route::get('cms/shop_products/edit/{id}', ['uses' => 'ShopProductsController@edit']);
    Route::get('cms/shop_products/list', ['uses' => 'ShopProductsController@list']);

    Route::get('cms/shop_orders/list', ['uses' => 'ShopOrdersController@list']);

//    Route::post('cms/shop_additional_products/store', ['uses' => 'ShopAdditionalProductsController@store']);
    Route::get('cms/shop_additional_products/edit/{id}', ['uses' => 'ShopAdditionalProductsController@edit']);
    Route::get('cms/shop_additional_products/list', ['uses' => 'ShopAdditionalProductsController@list']);

    Route::get('cms/{table}/add', ['uses' => 'WebsiteCmsController@add']);
    Route::post('cms/{table}/store', ['uses' => 'WebsiteCmsController@store']);
    Route::get('cms', ['uses' => 'WebsiteCmsController@start']);
});
