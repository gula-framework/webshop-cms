<?php


namespace Gula\WebsiteCms\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopTaxes extends Model
{
    protected $table = 'shop_taxes';
    protected $guarded = [];

    public function getTableName(){
        return $this->table;
    }

    public function getTaxes()
    {
        return DB::table($this->table)
            ->where('deleted', '=', false)
            ->orderBy('id', 'asc')
            ->get();
    }

}
