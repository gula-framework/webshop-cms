<?php


namespace Gula\WebsiteCms\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Pages extends Model
{
    protected $table = 'pages';
    protected $guarded = [];

    public function getOne(int $id){
        $page = DB::table($this->table)->where(['id' => $id])->first();
        $page->content = json_decode($page->content,1);

        return $page;
    }

    public function getTableName(){
        return $this->table;
    }

    public function getPages()
    {
        return DB::table($this->table)->where(['deleted' => false])->get();
    }

    public function store($post)
    {
        $id = $post['id'];
        unset($post['_token']);
        unset($post['id']);

        DB::table($this->table)
            ->where(['id' => $id])
            ->update(['content' => json_encode($post)]);
        $a=2;

    }

}
