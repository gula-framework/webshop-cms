<?php

namespace Gula\WebsiteCms\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ShopAdditionalProducts extends Model
{
    protected $table = 'shop_additional_products';
    protected $tableShopProductsShopAdditionalProducts = 'shop_products_shop_additional_products';

    protected $guarded = [];

    public function getOne(int $id){
        return DB::table($this->table)->where(['id' => $id])->first();
    }

    public function getTableName(){
        return $this->table;
    }

    public function getAdditionalProducts()
    {
        return DB::table($this->table)->where(['deleted' => false])->orderBy('name', 'asc')->get();
    }

    /**
     * @param int $idProduct
     * @return array
     */
    public function getLinkedProducts(int $idProduct): array
    {
        $linkedProducts = [];

        $result = DB::table($this->tableShopProductsShopAdditionalProducts)->select(['id_additional_product'])->where(['id_product' => $idProduct, 'deleted' => false])->get();

        foreach ($result as $item){
            $linkedProducts[] = $item->id_additional_product;
        }

        return $linkedProducts;
    }

}
